import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:great_places/utils/db_util.dart';
import 'package:great_places/utils/location_util.dart';

import '../models/place.dart';

class GreatPlaces with ChangeNotifier {
  List<Place> _items = [];

  Future<void> loadPlaces() async {
    await Future.delayed(const Duration(seconds: 1));
    final dataList = await DbUtil.getData('places');
    _items = dataList
        .map((place) => Place(
            id: place['id'].toString(),
            title: place['title'].toString(),
            latitude: double.parse(place['lat'].toString()),
            longitude: double.parse(place['lng'].toString()),
            address: place['address'].toString(),
            image: File(place['image'].toString())))
        .toList();
    notifyListeners();
  }

  List<Place> get items => [..._items];

  int get itemsCount => _items.length;

  Place getItem(int index) => _items[index];

  void addPlace(String title, File image, LatLng position) async{
    String address = await LocationUtil.getAddressFrom(position);

    final newPlace = Place(
      id: Random().nextDouble().toString(),
      title: title,
      latitude: position.latitude,
      longitude: position.longitude,
      address: address,
      image: image,
    );
    _items.add(newPlace);
    DbUtil.insert('places', {
      'id': newPlace.id,
      'title': newPlace.title,
      'image': newPlace.image.path,
      'lat': position.latitude,
      'lng': position.longitude,
      'address': address,
    });
    notifyListeners();
  }
}
