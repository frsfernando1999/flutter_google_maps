import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:great_places/screens/map_screen.dart';
import 'package:location/location.dart';

import '../utils/location_util.dart';

class LocationInput extends StatefulWidget {
  const LocationInput(this.onTap, {Key? key}) : super(key: key);

  final void Function(LatLng) onTap;

  @override
  State<LocationInput> createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  String? _previewImageUrl;

  Future<void> _getCurrentUserLocation() async {
    final locationData = await Location().getLocation();
    final staticMapUrl = LocationUtil.generateLocationPreviewImage(
      latitude: locationData.latitude,
      longitude: locationData.longitude,
    );
    setState(() {
      _previewImageUrl = staticMapUrl;
    });
    widget.onTap(LatLng(locationData.latitude!, locationData.longitude!));
  }

  Future<void> _selectOnMap() async {
    final LatLng? location = await Navigator.of(context)
        .push(MaterialPageRoute(fullscreenDialog: true,builder: (_) => MapScreen()));
    if(location == null) return;
    final staticMapUrl = LocationUtil.generateLocationPreviewImage(
      latitude: location.latitude,
      longitude: location.longitude,
    );
    setState(() {
      _previewImageUrl = staticMapUrl;
    });
    widget.onTap(location);
  }

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).colorScheme.primary;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          height: 170,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.grey),
          ),
          child: _previewImageUrl == null
              ? Text('Localização não informada')
              : Image.network(
                  _previewImageUrl!,
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton.icon(
              icon: Icon(
                Icons.location_on,
                color: primaryColor,
              ),
              label: Text(
                'Localização Atual',
                style: TextStyle(color: primaryColor),
              ),
              onPressed: _getCurrentUserLocation,
            ),
            TextButton.icon(
              icon: Icon(
                color: primaryColor,
                Icons.map,
              ),
              label: Text(
                'Selecionar no Mapa',
                style: TextStyle(color: primaryColor),
              ),
              onPressed: _selectOnMap,
            ),
          ],
        )
      ],
    );
  }
}
