import 'dart:io';

class Place{
  final String id;
  final String title;
  final double? latitude;
  final double? longitude;
  final String? address;
  final File image;

  const Place({
    required this.id,
    required this.title,
    required this.latitude,
    required this.longitude,
    this.address,
    required this.image,
  });

}