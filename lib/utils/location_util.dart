import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

const String googleApiKey = 'AIzaSyDLazzUd0wYxMoyMuqHZANimk95kl0qkwo';
const String googleApiUrl = 'https://maps.googleapis.com/maps/api/js?key=$googleApiKey';

class LocationUtil {
  static String generateLocationPreviewImage({
    double? longitude,
    double? latitude,
  }) {
    return "https://maps.googleapis.com/maps/api/staticmap?center=$latitude,$longitude&zoom=13&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:A%7C$latitude,$longitude&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:A%7C40.718217,-73.998284&key=$googleApiKey";
  }

  static Future<String> getAddressFrom(LatLng position) async{
    final String url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=$googleApiKey';
    final response = await http.get(Uri.parse(url));
    return json.decode(response.body)['results'][0]['formatted_address'];
  }
}