import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  const MapScreen(
      {this.latitude = -23.6821604,
      this.longitude = -46.87549151,
      this.readOnly = false,
      Key? key})
      : super(key: key);
  final double longitude;
  final double latitude;

  final bool readOnly;

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng? _pickedPosition;

  void _selectPosition(LatLng position) {
    setState(() {
      _pickedPosition = position;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.readOnly ? 'Mapa' : 'Selecione...'),
        actions: [
          widget.readOnly
              ? Container()
              : IconButton(
                  onPressed: () {
                    Navigator.of(context).pop(_pickedPosition);
                  },
                  icon: Icon(Icons.check))
        ],
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
            target: LatLng(widget.latitude, widget.longitude), zoom: 13),
        onTap: widget.readOnly ? null : _selectPosition,
        markers: widget.readOnly
            ? {
                Marker(
                    markerId: MarkerId('id1'),
                    position: LatLng(widget.latitude, widget.longitude))
              }
            : {
                Marker(
                    markerId: MarkerId('id1'),
                    position: _pickedPosition == null
                        ? LatLng(0.0, 0.0)
                        : _pickedPosition!)
              },
      ),
    );
  }
}
