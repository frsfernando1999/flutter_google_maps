import 'package:flutter/material.dart';
import 'package:great_places/providers/great_places.dart';
import 'package:great_places/screens/place_detail.dart';
import 'package:provider/provider.dart';

import '../utils/app_routes.dart';

class PlacesListScreen extends StatelessWidget {
  const PlacesListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meus Lugares'),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).pushNamed(AppRoutes.placeForm);
              },
              icon: Icon(Icons.add))
        ],
      ),
      body: FutureBuilder(
        future: context.read<GreatPlaces>().loadPlaces(),
        builder: (context, snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? Center(child: CircularProgressIndicator())
              : Consumer<GreatPlaces>(
                  child: Center(
                    child: Text('Nenhum Lugar'),
                  ),
                  builder: (context, greatPlaces, child) =>
                      greatPlaces.itemsCount != 0
                          ? ListView.builder(
                              itemBuilder: (context, index) {
                                var item = greatPlaces.items[index];
                                return ListTile(
                                  leading: CircleAvatar(
                                    backgroundImage: FileImage(item.image),
                                  ),
                                  title: Text(item.title),
                                  subtitle: Text('${item.address}'),
                                  onTap: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (_) =>
                                              PlaceDetailScreen(item))),
                                );
                              },
                              itemCount: greatPlaces.itemsCount,
                            )
                          : child!,
                );
        },
      ),
    );
  }
}
