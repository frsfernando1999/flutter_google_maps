import 'package:flutter/material.dart';
import 'package:great_places/models/place.dart';
import 'package:great_places/screens/map_screen.dart';

class PlaceDetailScreen extends StatelessWidget {
  const PlaceDetailScreen(this.place, {Key? key}) : super(key: key);
  final Place place;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(place.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height * 0.3),
                child: Image.file(
                  place.image,
                  fit: BoxFit.cover,
                )),
            SizedBox(height: 12),
            Text('${place.address}',
                textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
            SizedBox(height: 12),
            TextButton.icon(
              icon: Icon(Icons.map),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => MapScreen(
                          latitude: place.latitude!,
                          longitude: place.longitude!,
                      readOnly: true,
                        )));
              },
              label: Text('Ver no Mapa'),
            )
          ],
        ),
      ),
    );
  }
}
