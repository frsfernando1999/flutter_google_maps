import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:great_places/providers/great_places.dart';
import 'package:great_places/widgets/image_input.dart';
import 'package:great_places/widgets/location_input.dart';
import 'package:provider/provider.dart';

class PlaceFormScreen extends StatefulWidget {
  const PlaceFormScreen({Key? key}) : super(key: key);

  @override
  State<PlaceFormScreen> createState() => _PlaceFormScreenState();
}

class _PlaceFormScreenState extends State<PlaceFormScreen> {
  final TextEditingController titleController = TextEditingController();
  File? pickedImage;
  LatLng? pickedPosition;

  void _selectImage(File pickedImage) {
    this.pickedImage = pickedImage;
  }

  void _selectPosition(LatLng pickedPosition) {
    this.pickedPosition = pickedPosition;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo Lugar'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: [
                  TextField(
                    controller: titleController,
                    decoration: InputDecoration(
                      labelText: 'Título',
                    ),
                  ),
                  SizedBox(height: 12),
                  ImageInput(_selectImage),
                  SizedBox(height: 12),
                  LocationInput(_selectPosition),
                ],
              ),
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.symmetric(vertical: 12),
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                elevation: 0,
                primary: Theme.of(context).colorScheme.secondary,
              ),
              icon: Icon(
                Icons.add,
                color: Colors.black,
              ),
              onPressed: _submitForm,
              label: Text(
                'Adicionar',
                style: TextStyle(color: Colors.black),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _submitForm() {
    if (titleController.text.isEmpty || pickedImage == null || pickedPosition == null) {
      return;
    } else {
      context.read<GreatPlaces>().addPlace(
            titleController.text,
            pickedImage!,
            pickedPosition!
          );
    }
    Navigator.of(context).pop();
  }
}
